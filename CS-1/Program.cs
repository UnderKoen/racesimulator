﻿using System;

namespace CS_1 {
    internal class Program {
        public static void Main(string[] args) {
            Boot boot = new Boot();
            boot.Speed = 40;
            boot.Name = "Test";
            Console.WriteLine($"{boot.Name} heeft een maximale snelheid van {boot.Speed}");
        }
    }

    internal class Boot {
        private int _speed;
        private string _name;

        public int Speed {
            get => _speed;

            set {
                if (value < 0) throw new ArgumentOutOfRangeException($"{nameof(value)} needs to be higher than zero!");
                _speed = value;
            }
        }

        public string Name {
            get => $"The name is: {_name}";
            set {
                if (value.Length < 3) throw new Exception($"{nameof(value)} needs to be at least 3 chars long!");
                _name = value;
            }
        }
    }
}