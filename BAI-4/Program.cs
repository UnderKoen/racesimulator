﻿using System;

namespace BAI_4 {
    internal class Program {
        public static void Main(string[] args) {
            for (int i = -10; i <= 10; i++) {
                Console.WriteLine($"{i}\t{isEven(i)}");
            }
            for (int i = -10; i <= 10; i++) {
                Console.WriteLine($"{i}\t{isPos(i)}");
            }
        }

        public static bool isEven(int i) {
            return (i & 1) == 0;
        }

        public static bool isPos(int i) {
            return i >> 31 == 0;
        }
    }
}