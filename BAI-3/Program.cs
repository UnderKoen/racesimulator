﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BAI_3 {
    internal class Program {
        public static void Main(string[] args) {
            _calc("2 8 + 3 9 * + =");
            _calc("3 6 7 + * =");
            
            Dictionary<int, string> dict = new Dictionary<int, string>();
            System.IO.StreamReader file = new System.IO.StreamReader(@"../../namen.txt");
            file.ReadToEnd()
                .Split('\n')
                .Select(s => s.Split(' '))
                .Where(s => s.Length >= 2)
                .Select(s => (int.Parse(s[1]), s[0]))
                .ToList()
                .ForEach(g => {
                        if (!dict.ContainsKey(g.Item1)) dict[g.Item1] = g.Item2;
                        else Console.WriteLine($"Key bestaat al {g.Item1} = {g.Item2}");
                    }
                );
            
            Console.WriteLine($"1. Het aantal element in de dictionary is {dict.Count}");
            
            Enumerable.Range(1450, 21)
                .Select(i => (i, dict.ContainsKey(i) ? dict[i] : null))
                .ToList()
                .ForEach(g=> {
                        if (dict.Remove(g.i)) Console.WriteLine($"Key {g.i} wordt verwijdert: {g.Item2}");
                        else Console.WriteLine($"Key {g.i} bestaat niet");
                    }
                );
            
            Console.WriteLine($"3. Nu is get aantal element in de dictionary {dict.Count}");

            Console.WriteLine($"4. Unieke namen: {dict.Values.Distinct().Count()}");
        }

        private static void _calc(string s) {
            Stack<int> sum = new Stack<int>();
            foreach (char c in s) {
                if (char.IsDigit(c)) 
                    sum.Push(int.Parse(c.ToString()));
                else
                    switch (c) {
                        case '+':
                            sum.Push(sum.Pop() + sum.Pop());
                            break;
                        case '*':
                            sum.Push(sum.Pop() * sum.Pop());
                            break;
                        case '=':
                            Console.WriteLine(sum.Pop());
                            break;
                    }
            }
        }
    }
}