﻿using System;
using System.Linq;

namespace School {
    internal class Program {
        private const int Max = 10000000;
        private const int Players = 7;

        public static void Main(string[] args) {
            int[] players = new int[Players];
            int c = 0;
            bool dir = false;
            for (int i = 1; i <= Max; i++) {
                if (i % 7 == 0 || Contains7(i)) {
                    players[c]++;
                    dir = !dir;
                }

                c = dir ? (c == 0 ? Players - 1 : c - 1) : (c == Players - 1 ? 0 : c + 1);
            }

            for (int i = 0; i < Players; i++) {
                Console.WriteLine(
                    $"Speler {(char) ('A' + i)} aantal keren BUZZ:{players[i].ToString().PadLeft(14)}"
                );
            }

            Console.WriteLine("\t\t\t    ------------- +");

            Console.WriteLine($"Totaal   aantal keren BUZZ:{players.Sum().ToString().PadLeft(14)}");
            Console.WriteLine($"Totaal   aantal geen  BUZZ:{(Max - players.Sum()).ToString().PadLeft(14)}");

            Console.WriteLine("\t\t\t    ------------- +");

            Console.WriteLine($"Totaal\t\t\t  :{Max.ToString().PadLeft(14)}");
        }

        private static bool Contains7(int i) {
            while ((i /= 10) != 0) {
                if (i % 10 == 7) return true;
            }

            return false;
        }
    }
}