﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BAI_2 {
    internal static class Program {
        public static void Main(string[] args) {
            //Opgave 2
            const int max = 30;
            Random rnd = new Random();
            int[] rnds = Enumerable.Range(0, max).Select(i => rnd.Next(max)).ToArray();

            for (int i = 1; i <= rnds.Length; i++) {
                Console.Write($"{rnds[i - 1]}\t");
                if (i % 10 == 0) Console.WriteLine();
            }

            int[] n = new int[max];
            foreach (int i in rnds) n[i]++;
            
            int[][] min = n.Select((v, i) => new[] {i, v}).Where(i => i[1] > 0).ToArray();
            // Zelfde maar dan geen LINQ
            // int[][] min = new int[max][];
            // int j = 0;
            // for (var i = 0; i < n.Length; i++) {
            //     int v = n[i];
            //     if (v == 0) continue;
            //     min[j++] = new[] {i, v};
            // }
            
            Console.WriteLine($"{min[0][0]} komt {min[0][1]} keer voor");
            Console.WriteLine($"{min[1][0]} komt {min[1][1]} keer voor");

            //Huiswerk
            string[] names = {
                "arnhem",
                "amersfoort",
                "assen",
                "rotterdam",
                "utrect",
                "zwolle"
            };

            int[][] distance = {
                new[] {0, 50, 135, 120, 65, 65},
                new[] {50, 0, 140, 75, 20, 70},
                new[] {135, 140, 0, 220, 170, 75},
                new[] {120, 75, 220, 0, 55, 145},
                new[] {65, 20, 170, 55, 0, 90},
                new[] {65, 70, 75, 145, 90, 0},
            };

            string name = distance.Select((l, i) => (names[i], l.Sum()))
                .MinBy(v => v.Item2)
                // Alternative without custom extension uses O(n log n) where the extension is O(n)
                // .OrderBy(v => v.Item2)
                // .First()
                .Item1;

            Console.WriteLine(name);
        }

        static T MinBy<T, R>(this IEnumerable<T> en, Func<T, R> evaluate) where R : IComparable<R> {
            return en.Select(t => new Tuple<T, R>(t, evaluate(t)))
                .Aggregate((max, next) => next.Item2.CompareTo(max.Item2) < 0 ? next : max).Item1;
        }
    }
}