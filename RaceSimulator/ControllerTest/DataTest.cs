﻿using System;
using System.Collections.Generic;
using Controller;
using Model;
using NUnit.Framework;

namespace ControllerTest {
    public class DataTest {
        [SetUp]
        public void Setup() {
            Data.Init();
        }
        
        [Test]
        public void CreatesCompetition() {
            Assert.IsNotNull(Data.Competition);
        }
        
        [Test]
        public void CreatesTracks() {
            Assert.IsNotNull(Data.Competition.Tracks);
            Assert.IsNotEmpty(Data.Competition.Tracks);
        }
        
        [Test]
        public void CreatesParticipants() {
            Assert.IsNotNull(Data.Competition.Participants);
            Assert.IsNotEmpty(Data.Competition.Participants);
        }
        
        [Test]
        public void CanRace() {
            Assert.IsTrue(Data.HasNextRace());
        }
        
        [Test]
        public void RaceStartNull() {
            Assert.IsNull(Data.CurrentRace);
        }
        
        [Test]
        public void NoTracksLeft() {
            Data.Competition.Tracks.Clear();
            Data.NextRace();
            Assert.IsNull(Data.CurrentRace);
        }
        
        [Test]
        public void BestDataNoData() {
            ParticipantData<TimeSpan> data = Data.Competition.TotalTime.Best(Comparer<TimeSpan>.Default);
            
            Assert.IsNotNull(data);
            Assert.IsNull(data.Participant);
            Assert.AreEqual(data.Value, default);
        }
    }
}