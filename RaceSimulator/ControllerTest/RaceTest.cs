﻿using System.Collections.Generic;
using System.Linq;
using Controller;
using Model;
using NUnit.Framework;

namespace ControllerTest {
    public class RaceTest {
        [SetUp]
        public void Setup() {
            Data.Init();
            Data.NextRace();
        }
        
        [Test]
        public void CreatesRace() {
            Assert.IsNotNull(Data.CurrentRace);
        }
        
        [Test]
        public void AddsParticipants() {
            Race race = Data.CurrentRace;
            Competition competition = Data.Competition;

            Assert.AreEqual(race.Participants, competition.Participants);
        }
        
        [Test]
        public void PlayersHavePosition() {
            Race race = Data.CurrentRace;

            Assert.AreEqual(race.PlayerPositions.Keys.Distinct().Count(), race.Participants.Count);
        }
        
        [Test]
        public void PlayersStartAtStart() {
            Race race = Data.CurrentRace;
            List<SectionType> types = race.PlayerPositions.Values.Select(section => section.Type).ToList();
            
            Assert.IsFalse(types.Any(type => type != SectionType.StartGrid));
        }

        [Test]
        public void DriversChangedIsCalled() {
            Race race = Data.CurrentRace;
            bool isCalled = false;

            race.DriverChanged += (o, args) => isCalled = true;
            race.OnTimer(null, null);
            
            Assert.IsTrue(isCalled);
        }

        [Test]
        public void RaceEndsWithinThousandExecutes() {
            Data.NextRace();
            Race race = Data.CurrentRace;
            bool hasEnded = false;

            race.RaceEnded += (o, args) => hasEnded = true;
            for (int i = 0; i < 1000 && !hasEnded; i++) {
                race.OnTimer(null, null);                
            }

            Assert.IsTrue(hasEnded);
        }
    }
}