﻿using System;
using Model;
using NUnit.Framework;

namespace ControllerTest {
    [TestFixture]
    public class CompetitionTest {
        private Competition _competition;

        [SetUp]
        public void Setup() {
            _competition = new Competition();
        }
        
        [Test]
        public void NextTrackEmpty() {
            Track track = _competition.NextTrack();
            Assert.IsNull(track);
        }
        
        [Test]
        public void NextTrackOne() {
            Track track = new Track("Monaco", new SectionType[]{});
            _competition.Tracks.Enqueue(track);
            
            Track result = _competition.NextTrack();
            Assert.AreEqual(track, result, "Track was not the same as injected");
            
            result = _competition.NextTrack();
            Assert.IsNull(result, "Track was not removed from queue");
        }
        
        [Test]
        public void NextTrackTwo() {
            Track track = new Track("Monaco", new SectionType[]{});
            Track track2 = new Track("Spa-Francorchamps", new SectionType[]{});
            _competition.Tracks.Enqueue(track);
            _competition.Tracks.Enqueue(track2);
            
            Track result = _competition.NextTrack();
            Assert.AreEqual(track, result, "Track 1/2 was not the same as injected");

            result = _competition.NextTrack();
            Assert.AreEqual(track2, result, "Track 2/2 was not the same as injected");
        }
        
        [Test]
        public void DataStartsEmpty() {
            Assert.NotNull(_competition.GetBestPoints());
            Assert.NotNull(_competition.GetTotalTime());
            
            Assert.AreEqual(_competition.GetBestPoints().Value, default);
            Assert.AreEqual(_competition.GetTotalTime().Value, default);
            
            Assert.IsNull(_competition.GetBestPoints().Participant);
            Assert.IsNull(_competition.GetTotalTime().Participant);
        }
    }
}