﻿using Controller;
using NUnit.Framework;

namespace ControllerTest {
    public class RaceDataTest {
        [SetUp]
        public void Setup() {
            Data.Init();
            Data.NextRace();
            
            Data.NextRace();
            Race race = Data.CurrentRace;
            bool hasEnded = false;

            race.RaceEnded += (o, args) => hasEnded = true;
            for (int i = 0; i < 1000 && !hasEnded; i++) {
                race.OnTimer(null, null);                
            }
            
            Assert.IsTrue(hasEnded);
        }

        [Test]
        public void RoundTimesAdded() {
            Race race = Data.CurrentRace;

            Assert.GreaterOrEqual(race.RoundTime.data.Count, race.Participants.Count);
        }

        [Test]
        public void BestTimeNotNull() {
            Race race = Data.CurrentRace;

            Assert.NotNull(race.GetBestRound());
        }

        [Test]
        public void FarthestNotNull() {
            Race race = Data.CurrentRace;

            Assert.NotNull(race.GetFarthest());
        }
    }
}