﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Controller;
using Model;

namespace RaceSimulator {
    public static class Visualization {
        #region sections

        private static readonly string[] Finish = {
            "@@@@",
            " :a ",
            "b:  ",
            "@@@@",
        };

        private static readonly string[] Start = {
            "@@@@",
            "  a|",
            "b|  ",
            "@@@@",
        };

        private static readonly string[] Straight = {
            "@@@@",
            "  a ",
            "b   ",
            "@@@@",
        };

        private static readonly string[] CornerRight = {
            "@@==",
            "  @=",
            "b  @",
            "  a@",
        };

        private static readonly string[] CornerLeft = {
            " a @",
            "   @",
            "b @=",
            "@@==",
        };

        private static readonly string[] Empty = {
            "====",
            "====",
            "====",
            "====",
        };

        #endregion

        private static string[] _getSectionLines(Section section) {
            if (section == null) return Empty;
            switch (section.Type) {
                case SectionType.Straight: return Straight;
                case SectionType.LeftCorner: return CornerLeft;
                case SectionType.RightCorner: return CornerRight;
                case SectionType.StartGrid: return Start;
                case SectionType.Finish: return Finish;
                default: return Empty;
            }
        }

        private static string[] _rot90(string[] lines) {
            char[][] n = {new char[4], new char[4], new char[4], new char[4]};
            for (int i = 0; i < 16; i++) {
                n[i / 4][i % 4] = lines[i % 4][3 - i / 4];
            }

            return n.Select(cl => new string(cl)).ToArray();
        }

        private static string[] _rot180(string[] lines) {
            return lines.Select(l => new string(l.Reverse().ToArray())).Reverse().ToArray();
        }

        private static string[] placePlayer(string place, string[] lines, IParticipant player) {
            string replace = " ";
            if (player != null) {
                replace = player.Name[0].ToString();
                if (player.Equipment.IsBroken) replace = replace.ToLower();
            }

            for (var i = 0; i < lines.Length; i++) {
                lines[i] = lines[i].Replace(place, replace);
            }

            return lines;
        }

        private static string[] _getSection(Section section, SectionData data) {
            if (section == null) return Empty;

            string[] lines = _getSectionLines(section).ToArray();
            lines = placePlayer("a", lines, data.Left);
            lines = placePlayer("b", lines, data.Right);

            switch (section.Dir) {
                case 0: return lines;
                case 1: return _rot90(_rot180(lines));
                case 2: return _rot180(lines);
                case 3: return _rot90(lines);
            }

            throw new Exception();
        }

        public static void DrawRace(
            Race race,
            ParticipantData<int> points,
            ParticipantData<int> distance,
            ParticipantData<TimeSpan> bestRound,
            ParticipantData<TimeSpan> totalTime
        ) {
            Console.CursorTop = 1;
            Console.CursorLeft = 0;
            Console.WriteLine(race.Track.Name);
            WriteData(points, "pts", "Most points in Competition");
            WriteData(totalTime, "", "Best combined timed of the Competition");
            WriteData(distance, "M", "Most distance current race");
            WriteData(bestRound, "", "Best round of current race");
            DrawTrack(race);
        }

        public static void WriteData<T>(ParticipantData<T> data, string suffix, string hint) {
            Console.WriteLine(
                $"{data.Participant.Name.PadLeft(8)}:" +
                $"{data.Value.ToString().PadLeft(24)}" +
                $" {suffix.PadRight(6)}" +
                $"//{hint}"
            );
        }

        public static void DrawTrack(Race race) {
            Track track = race.Track;

            Dictionary<Position, Section> grid = new Dictionary<Position, Section>();
            int x = 0, y = 0, d = 0;

            foreach (Section section in track.Sections) {
                grid[new Position(x, y)] = section;
                section.Dir = d;

                if (section.Type == SectionType.LeftCorner) d--;
                else if (section.Type == SectionType.RightCorner) d++;

                //Non negative modulo (a % b only works for b >= 0)
                d = (d %= 4) < 0 ? d + 4 : d;
                x += (d - 1) * -1 % 2;
                y += (d - 2) * -1 % 2;
            }

            Position min = grid.Keys.Aggregate((p1, p2) => new Position(Math.Min(p1.X, p2.X), Math.Min(p1.Y, p2.Y)));
            Position max = grid.Keys.Aggregate((p1, p2) => new Position(Math.Max(p1.X, p2.X), Math.Max(p1.Y, p2.Y)));

            List<StringBuilder> lines = new List<StringBuilder>();
            int l = 0;
            for (y = min.Y; y <= max.Y; y++) {
                for (int i = 0; i < 4; i++) {
                    lines.Add(new StringBuilder());
                }

                for (x = min.X; x <= max.X; x++) {
                    Position pos = new Position(x, y);
                    Section section = grid.ContainsKey(pos) ? grid[pos] : null;
                    string[] ls = _getSection(section, race.GetSectionData(section));
                    for (var i = 0; i < 4; i++) {
                        lines[l + i].Append(ls[i]);
                    }
                }

                l += 4;
            }

            //TODO fix memory stuff
            Console.Write(
                new string(
                    lines.SelectMany(
                        line => line.ToString()
                            .SelectMany(c => new[] {c, c})
                            .Append('\n')
                    ).ToArray()
                )
            );
        }
    }

    public class Position {
        public int X { get; }
        public int Y { get; }

        public Position(int x, int y) {
            X = x;
            Y = y;
        }

        public Position((int x, int y) p) {
            X = p.x;
            Y = p.y;
        }

        private bool Equals(Position other) { return X == other.X && Y == other.Y; }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Position) obj);
        }

        public override int GetHashCode() {
            unchecked {
                return (X * 397) ^ Y;
            }
        }

        public override string ToString() { return $"{nameof(X)}: {X}, {nameof(Y)}: {Y}"; }
    }
}