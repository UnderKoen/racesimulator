﻿using System;
using Model;

namespace Controller {
    public static class Data {
        public static Competition Competition { get; set; }
        public static Race CurrentRace { get; set; }

        public static void Init() {
            Competition = new Competition();
            AddParticipants();
            AddTracks();
        }

        public static void AddParticipant(IParticipant participant) {
            Competition.Participants.Add(participant);
            Competition.Points.Add(participant, 0);
        }

        public static void AddParticipants() {
            AddParticipant(new Driver("Klaas", TeamColor.Blue));
            AddParticipant(new Driver("Jan", TeamColor.Green));
            AddParticipant(new Driver("Paul", TeamColor.Grey));
            AddParticipant(new Driver("Arvid", TeamColor.Yellow));
            AddParticipant(new Driver("Daan", TeamColor.Red));
        }

        public static void AddTrack(Track track) { Competition.Tracks.Enqueue(track); }

        public static void AddTracks() {
            #region tracks

            // AddTrack(
            //     new Track(
            //         "Oosterndorp",
            //         new[] {
            //             SectionType.StartGrid,
            //             SectionType.StartGrid, SectionType.StartGrid, SectionType.Finish, SectionType.Straight,
            //             SectionType.LeftCorner, SectionType.Straight, SectionType.Straight, SectionType.Straight,
            //             SectionType.RightCorner, SectionType.Straight, SectionType.Straight, SectionType.RightCorner,
            //             SectionType.Straight, SectionType.RightCorner, SectionType.Straight, SectionType.Straight,
            //             SectionType.Straight, SectionType.Straight, SectionType.Straight, SectionType.Straight,
            //             SectionType.Straight, SectionType.Straight, SectionType.Straight, SectionType.LeftCorner,
            //             SectionType.Straight, SectionType.Straight, SectionType.Straight, SectionType.Straight,
            //             SectionType.LeftCorner, SectionType.Straight, SectionType.Straight, SectionType.Straight,
            //             SectionType.RightCorner, SectionType.RightCorner, SectionType.LeftCorner,
            //             SectionType.LeftCorner, SectionType.RightCorner, SectionType.RightCorner, SectionType.Straight,
            //             SectionType.Straight, SectionType.Straight, SectionType.Straight, SectionType.Straight,
            //             SectionType.Straight, SectionType.RightCorner, SectionType.Straight, SectionType.Straight,
            //             SectionType.Straight, SectionType.Straight, SectionType.Straight, SectionType.RightCorner,
            //             SectionType.Straight, SectionType.Straight, SectionType.Straight, SectionType.Straight
            //         }
            //     )
            // );


            AddTrack(
                new Track(
                    "Sprint Unfair",
                    new[] {
                        SectionType.RightCorner,
                        SectionType.LeftCorner,
                        SectionType.StartGrid,
                        SectionType.StartGrid,
                        SectionType.LeftCorner,
                        SectionType.LeftCorner,
                        SectionType.StartGrid,
                        SectionType.Finish,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.RightCorner,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.RightCorner,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.RightCorner,
                        SectionType.RightCorner,
                        SectionType.LeftCorner,
                        SectionType.Straight,
                        SectionType.LeftCorner,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.LeftCorner,
                        SectionType.Finish,
                        SectionType.Finish,
                        SectionType.RightCorner,
                        SectionType.Finish,
                        SectionType.Finish,
                        SectionType.RightCorner,
                        SectionType.Finish,
                        SectionType.Finish,
                        SectionType.RightCorner,
                        SectionType.RightCorner,
                        SectionType.RightCorner,
                    }
                )
            );

            AddTrack(
                new Track(
                    "Circuit Aquaduct",
                    new[] {
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.RightCorner,
                        SectionType.Straight,
                        SectionType.RightCorner,
                        SectionType.RightCorner,
                        SectionType.StartGrid,
                        SectionType.StartGrid,
                        SectionType.StartGrid,
                        SectionType.Finish,
                        SectionType.LeftCorner,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.LeftCorner,
                        SectionType.LeftCorner,
                        SectionType.RightCorner,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.RightCorner,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.RightCorner,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.RightCorner,
                        SectionType.Straight,
                        SectionType.RightCorner,
                        SectionType.LeftCorner,
                    }
                )
            );

            AddTrack(
                new Track(
                    "Spa-Francorchamps", new[] {
                        SectionType.StartGrid,
                        SectionType.StartGrid,
                        SectionType.StartGrid,
                        SectionType.Finish,
                        SectionType.LeftCorner,
                        SectionType.RightCorner,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.RightCorner,
                        SectionType.Straight,
                        SectionType.RightCorner,
                        SectionType.LeftCorner,
                        SectionType.LeftCorner,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.LeftCorner,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.LeftCorner,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.LeftCorner,
                        SectionType.Straight,
                        SectionType.RightCorner,
                        SectionType.Straight,
                        SectionType.RightCorner,
                        SectionType.LeftCorner,
                        SectionType.LeftCorner,
                        SectionType.Straight,
                        SectionType.LeftCorner,
                    }
                )
            );

            AddTrack(
                new Track(
                    "Circuit Elburg",
                    new[] {
                        SectionType.StartGrid,
                        SectionType.StartGrid,
                        SectionType.StartGrid,
                        SectionType.Finish,
                        SectionType.RightCorner,
                        SectionType.Straight,
                        SectionType.LeftCorner,
                        SectionType.Straight,
                        SectionType.RightCorner,
                        SectionType.RightCorner,
                        SectionType.Straight,
                        SectionType.LeftCorner,
                        SectionType.RightCorner,
                        SectionType.Straight,
                        SectionType.LeftCorner,
                        SectionType.RightCorner,
                        SectionType.RightCorner,
                        SectionType.LeftCorner,
                        SectionType.Straight,
                        SectionType.RightCorner,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.RightCorner
                    }
                )
            );

            AddTrack(
                new Track(
                    "Monaco", new[] {
                        SectionType.StartGrid,
                        SectionType.StartGrid,
                        SectionType.StartGrid,
                        SectionType.Finish,
                        SectionType.LeftCorner,
                        SectionType.Straight,
                        SectionType.LeftCorner,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.Straight,
                        SectionType.LeftCorner,
                        SectionType.Straight,
                        SectionType.LeftCorner,
                    }
                )
            );

            #endregion
        }

        public static void NextRace() {
            Track track = Competition.NextTrack();
            if (track == null) return;
            CurrentRace = new Race(track, Competition.Participants);
            CurrentRace.RaceEnded += _addScore;
        }

        private static void _addScore(object o, RaceEventArgs args) {
            Race race = args.Race;
            for (var i = 0; i < race.Finished.Count; i++) {
                IParticipant participant = race.Finished[i];
                int points = (Competition.Participants.Count - i) * 2;
                points += Competition.Points.Get(participant);
                Competition.Points.Replace(participant, points);

                TimeSpan time = Competition.TotalTime.Get(participant);
                foreach (TimeSpan timeSpan in race.RoundTime.GetAll(participant)) {
                    time = time.Add(timeSpan);
                }

                Competition.TotalTime.Replace(participant, time);
            }
        }

        public static bool HasNextRace() { return Competition.Tracks.Count > 0; }
    }
}