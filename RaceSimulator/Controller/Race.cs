﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using Model;

namespace Controller {
    public delegate void DriverChanged(object o, RaceEventArgs args);

    public delegate void RaceEnded(object o, RaceEventArgs args);

    public class Race {
        private const int BaseSpeed = 25;
        private const int SectionLength = 256;
        private const int MinPerformace = 2;
        private const int MaxPerformace = 10;
        private const int MinQuality = 1;
        private const int MaxQuality = 10;
        private const int MaxLaps = 3;
        private const int Timer = 100;
        private const int BrokenChance = 1;
        private const int FixChance = 20;

        public DataHolder<TimeSpan> RoundTime = new DataHolder<TimeSpan>();
        public DataHolder<int> Distance = new DataHolder<int>();
        public DataHolder<int> Rounds = new DataHolder<int>();

        private Random _random;
        private Timer _timer;
        private int _loop = 0;
        private Dictionary<Section, SectionData> _positions = new Dictionary<Section, SectionData>();
        public readonly List<IParticipant> Finished = new List<IParticipant>();

        public Track Track { get; set; }
        public List<IParticipant> Participants { get; set; }
        public DateTime StartTime { get; set; }
        public event DriverChanged DriverChanged;
        public event RaceEnded RaceEnded;

        public ParticipantData<TimeSpan> GetBestRound() {
            return RoundTime.Best(Comparer<TimeSpan>.Create((t1, t2) => t1.CompareTo(t2)));
        }

        public ParticipantData<int> GetFarthest() {
            return Distance.Best(Comparer<int>.Create((t1, t2) => t2.CompareTo(t1)));
        }

        public Dictionary<IParticipant, Section> PlayerPositions;
        public Dictionary<IParticipant, int> Laps;
        public Dictionary<IParticipant, int> PlayerTimes = new Dictionary<IParticipant, int>();

        public Race(Track track, List<IParticipant> participants) {
            Track = track;
            Participants = participants;
            StartTime = DateTime.Now;
            _random = new Random(StartTime.Millisecond);
            _timer = new Timer(Timer);
            PlayerPositions = new Dictionary<IParticipant, Section>();
            Laps = new Dictionary<IParticipant, int>();
            SetStartPositions();
            RandomizeEquipment();
            _timer.Elapsed += OnTimer;
        }

        public void OnTimer(object o, EventArgs args) {
            _loop++;
            foreach (KeyValuePair<IParticipant, Section> data in new Dictionary<IParticipant, Section>(PlayerPositions)
            ) {
                IParticipant player = data.Key;
                IEquipment equipment = player.Equipment;

                if (equipment.IsBroken) {
                    if (_random.Next(100) <= FixChance) {
                        equipment.IsBroken = false;
                    }

                    continue;
                }

                if (_random.Next(100) <= BrokenChance) {
                    equipment.IsBroken = true;
                }

                int speed = equipment.Performance * equipment.Speed;
                int t = Distance.Get(player);
                Distance.Replace(player, t + speed);

                Section section = data.Value;
                SectionData sData = GetSectionData(section);
                int distance = sData.GetDistance(player);
                distance += speed;

                RemovePosition(player, section);
                for (var node = Track.Sections.Find(section); (distance -= SectionLength) > 0;) {
                    if (node == null) break;
                    node = node.Next ?? Track.Sections.First;
                    Section c = node.Value;
                    if (c.Type == SectionType.Finish) _nextLap(player);
                    if (GetSectionData(c).spaceLeft) section = c;
                }

                distance += SectionLength;

                if (Laps[player] <= MaxLaps) SetPosition(player, section, distance);
            }

            if (PlayerPositions.Count == 0) _endRace();

            DriverChanged?.Invoke(o, new RaceEventArgs(this));
        }

        private void _nextLap(IParticipant participant) {
            int lap = ++Laps[participant];
            Rounds.Replace(participant, lap);
            if (lap > 0) {
                int l = _loop - PlayerTimes[participant];
                RoundTime.Add(participant, TimeSpan.FromMilliseconds(l * Timer));
                PlayerTimes[participant] = _loop;
            }

            if (Laps[participant] == MaxLaps) Finished.Add(participant);
        }

        private void _endRace() {
            RaceEnded?.Invoke(this, new RaceEventArgs(this));
        }

        public void Start() {
            _timer.Start();
        }

        public void Stop() {
            _timer.Stop();
            DriverChanged = null;
            RaceEnded = null;
        }

        public SectionData GetSectionData(Section section) {
            if (section == null) return null;
            if (!_positions.ContainsKey(section)) _positions[section] = new SectionData();
            return _positions[section];
        }

        public void RandomizeEquipment() {
            foreach (IParticipant p in Participants) {
                IEquipment equipment = p.Equipment;
                equipment.Performance = _random.Next(MinPerformace, MaxPerformace);
                equipment.Quality = _random.Next(MinQuality, MaxQuality);
                equipment.Speed = BaseSpeed;
                Laps[p] = -1;
                PlayerTimes[p] = 0;
            }
        }

        public void SetStartPositions() {
            List<Section> sections = Track.Sections
                .Where(s => s.Type == SectionType.StartGrid)
                .Reverse()
                .ToList();

            Participants
                .Select((p, i) => (s: sections[i / 2], p))
                .ToList()
                .ForEach(t => SetPosition(t.p, t.s, 20 + 128));
        }

        public void RemovePosition(IParticipant player, Section section) {
            if (player == null) return;
            PlayerPositions.Remove(player);
            SectionData data = GetSectionData(section);
            if (data.Left == player) {
                data.Left = null;
            } else if (data.Right == player) {
                data.Right = null;
            }
        }

        public void SetPosition(IParticipant player, Section section, int distance) {
            if (player == null) return;

            SectionData data = GetSectionData(section);
            PlayerPositions[player] = section;
            if (data.Left == null) {
                data.Left = player;
                data.DistanceLeft = distance;
            } else if (data.DistanceLeft > distance) {
                data.Right = player;
                data.DistanceRight = distance;
            } else {
                data.Right = data.Left;
                data.Left = player;
                data.DistanceRight = data.DistanceLeft;
                data.DistanceLeft = distance;
            }
        }
    }
}