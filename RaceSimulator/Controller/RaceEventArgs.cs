using System;

namespace Controller {
    public class RaceEventArgs : EventArgs {
        public readonly Race Race;

        public RaceEventArgs(Race race) {
            Race = race;
        }
    }
}