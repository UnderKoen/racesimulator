﻿namespace Model {
    public class Section {
        public SectionType Type { get; set; }
        public int Dir { get; set; }

        public Section(SectionType type) {
            Type = type;
        }
    }
}