﻿namespace Model {
    public enum SectionType {
        Straight,
        LeftCorner,
        RightCorner,
        StartGrid,
        Finish
    }
}