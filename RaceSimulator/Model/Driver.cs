﻿namespace Model {
    public class Driver: IParticipant {
        public string Name { get; set; }
        public int Points { get; set; }
        public IEquipment Equipment { get; set; }
        public TeamColor TeamColor { get; set; }

        public Driver(string name, TeamColor teamColor) {
            Name = name;
            TeamColor = teamColor;
            Points = 0;
            Equipment = new Car();
        }
    }
}