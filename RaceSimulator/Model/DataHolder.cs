using System;
using System.Collections.Generic;
using System.Linq;

namespace Model {
    public class DataHolder<T> {
        public List<ParticipantData<T>> data = new List<ParticipantData<T>>();

        public void Add(IParticipant participant, T value) {
            data.Add(new ParticipantData<T>(participant, value));
        }

        public void Replace(IParticipant participant, T value) {
            data.RemoveAll(d => d.Participant == participant);
            Add(participant, value);
        }

        public T Get(IParticipant participant) {
            ParticipantData<T> temp = data.FindLast(d => d.Participant == participant);
            return temp == null ? default : temp.Value;
        }

        public List<T> GetAll(IParticipant participant) {
            return data.FindAll(d => d.Participant == participant)
                       .Select(data => data.Value)
                       .ToList();
        }

        public ParticipantData<T> Best(Comparer<T> comparer) {
            if (data.Count == 0) return new ParticipantData<T>(default, default);
            return data.OrderBy(d => d.Value, comparer).First();
        }
    }
}