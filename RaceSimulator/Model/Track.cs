﻿using System.Collections.Generic;
using System.Linq;

namespace Model {
    public class Track {
        public string Name { get; set; }
        public LinkedList<Section> Sections { get; set; }

        public Track(string name, SectionType[] sections) {
            Name = name;
            Sections = ConvertTrack(sections);
        }

        public LinkedList<Section> ConvertTrack(SectionType[] sectionTypes) {
            return new LinkedList<Section>(sectionTypes.Select(type => new Section(type)));
        }
    }
}