﻿namespace Model {
    public class SectionData {
        public IParticipant Left { get; set; }
        public int DistanceLeft { get; set; }
        
        public IParticipant Right { get; set; }
        public int DistanceRight { get; set; }

        public int GetDistance(IParticipant participant) {
            if (Left == participant) return DistanceLeft;
            if (Right == participant) return DistanceRight;
            return -1;
        }

        public bool spaceLeft => Left == null || Right == null;
    }
}