﻿using System;
using System.Collections.Generic;

namespace Model {
    public class Competition {
        public DataHolder<int> Points = new DataHolder<int>();
        public DataHolder<TimeSpan> TotalTime = new DataHolder<TimeSpan>();
        public List<IParticipant> Participants { get; set; }
        public Queue<Track> Tracks { get; set; }

        public Competition() {
            Participants = new List<IParticipant>();
            Tracks = new Queue<Track>();
        }

        public Track NextTrack() {
            return Tracks.Count == 0 ? null : Tracks.Dequeue();
        }
        
        public ParticipantData<TimeSpan> GetTotalTime() {
            return TotalTime.Best(Comparer<TimeSpan>.Create((t1, t2) => t1.CompareTo(t2)));
        }
        
        public ParticipantData<int> GetBestPoints() {
            return Points.Best(Comparer<int>.Create((t1, t2) => t2.CompareTo(t1)));
        }
    }
}