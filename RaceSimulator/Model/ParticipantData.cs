using System.Collections.Generic;
using System.Linq;

namespace Model {
    public class ParticipantData<T> {
        public IParticipant Participant { get; set; }
        public T Value { get; set; }

        public ParticipantData(IParticipant participant, T value) {
            Participant = participant;
            Value = value;
        }
    }
}