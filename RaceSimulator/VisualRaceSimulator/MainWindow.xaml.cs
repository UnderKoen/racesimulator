﻿using System;
using System.Windows;
using System.Windows.Threading;
using Controller;

namespace VisualRaceSimulator {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow {
        private RaceInfo _raceInfo;
        private PlayerInfo _playerInfo;

        public static event DriverChanged change;
        
        public MainWindow() {
            InitializeComponent();
            Data.Init();
            NextRace();
            change += ((Test) DataContext).DriverChanged;
        }

        public void StartRace(Race race) {
            DrawCurrent();
            race.DriverChanged += (o, eventArgs) => {
                DrawCurrent();
                change?.Invoke(o, eventArgs);
            };
            race.RaceEnded += (o, eventArgs) => {
                race.Stop();
                NextRace();
            };
            race.Start();
        }

        public void NextRace() {
            if (Data.HasNextRace()) {
                Data.NextRace();
                Image.Dispatcher.BeginInvoke(
                    DispatcherPriority.Render, new Action(
                        () => {
                            Track.Source = null;
                            Track.Source = Visualisation.DrawTrack(Data.CurrentRace.Track)
                                .CreateBitmapSourceFromGdiBitmap();
                            Image.Source = null;
                            Image.Source = Visualisation.DrawParticipants(Data.CurrentRace)
                                .CreateBitmapSourceFromGdiBitmap();
                            StartRace(Data.CurrentRace);
                        }
                    )
                );
            }
        }

        public void DrawCurrent() {
            Image.Dispatcher.BeginInvoke(
                DispatcherPriority.Render, new Action(
                    () => {
                        Image.Source = null;
                        Image.Source = Visualisation.DrawParticipants(Data.CurrentRace)
                            .CreateBitmapSourceFromGdiBitmap();
                    }
                )
            );
        }

        private void MenuItem_Exit_Click(object sender, System.Windows.RoutedEventArgs e) {
            Application.Current.Shutdown();
        }

        private void MenuItem_Players_Click(object sender, System.Windows.RoutedEventArgs e) {
            _playerInfo = new PlayerInfo {DataContext = DataContext};
            _playerInfo.Show();
        }

        private void MenuItem_Race_Click(object sender, System.Windows.RoutedEventArgs e) {
            _raceInfo = new RaceInfo() {DataContext = DataContext};
            _raceInfo.Show();
        }
    }
}