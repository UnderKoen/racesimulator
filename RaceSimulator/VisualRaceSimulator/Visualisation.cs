﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Controller;
using Model;
using RaceSimulator;

namespace VisualRaceSimulator {
    public static class Visualisation {
        public const int TileSize = 256;

        public static readonly int[,,] Positions = {
            {{0, 58}, {0, 158}},
            {{158, 0}, {58, 0}},
            {{256, 158}, {256, 58}},
            {{58, 256}, {158, 256}},
        };

        public static Asset DrawParticipants(Race race) {
            Track track = race.Track;
            Dictionary<Position, Section> grid = new Dictionary<Position, Section>();
            int x = 0, y = 0, d = 0;

            foreach (Section section in track.Sections) {
                grid[new Position(x, y)] = section;
                section.Dir = d;

                if (section.Type == SectionType.LeftCorner) d--;
                else if (section.Type == SectionType.RightCorner) d++;

                //Non negative modulo (a % b only works for b >= 0)
                d = (d %= 4) < 0 ? d + 4 : d;
                x += (d - 1) * -1 % 2;
                y += (d - 2) * -1 % 2;
            }

            Position min = grid.Keys.Aggregate(
                (p1, p2) => new Position(Math.Min(p1.X, p2.X), Math.Min(p1.Y, p2.Y))
            );
            Position max = grid.Keys.Aggregate(
                (p1, p2) => new Position(Math.Max(p1.X, p2.X), Math.Max(p1.Y, p2.Y))
            );

            int w = max.X - min.X + 1;
            int h = max.Y - min.Y + 1;

            Bitmap canvas = Asset.CreateEmpty(w * TileSize, h * TileSize);
            Graphics graphics = Graphics.FromImage(canvas);

            int cx = 0, cy = 0;
            for (y = min.Y; y <= max.Y; y++) {
                for (x = min.X; x <= max.X; x++) {
                    Position pos = new Position(x, y);
                    Section section = grid.ContainsKey(pos) ? grid[pos] : null;
                    cx++;
                    if (section == null) continue;

                    SectionData data = race.GetSectionData(section);
                    if (data.Left != null) {
                        int[] offset = getOffset(section, data.DistanceLeft);
                        // graphics.FillRectangle(
                        //     Brushes.Red, (cx - 1) * TileSize + 32,
                        //     cy * TileSize + 32, 256 - 64, 254 - 64
                        // );
                        Point point = new Point(
                            (cx - 1) * TileSize + Positions[section.Dir, 0, 0] + offset[0],
                            cy * TileSize + Positions[section.Dir, 0, 1] + offset[1]
                        );

                        if (data.Left.Equipment.IsBroken) {
                            graphics.DrawImage(Asset.Oil.Image, point);
                        }
                        graphics.DrawImage(Asset.Cars[data.Left.TeamColor].Directed(section.Dir).Image, point);
                    }

                    if (data.Right != null) {
                        int[] offset = getOffset(section, data.DistanceRight);
                        graphics.DrawImage(
                            Asset.Cars[data.Right.TeamColor].Directed(section.Dir).Image,
                            new Point(
                                (cx - 1) * TileSize + Positions[section.Dir, 1, 0] + offset[0],
                                cy * TileSize + Positions[section.Dir, 1, 1] + offset[1]
                            )
                        );
                    }
                }

                cx = 0;
                cy++;
            }

            return new Asset("", canvas);
        }

        public static int[] getOffset(Section section, int distance) {
            int d = section.Dir;
            return new[] {(distance - 128) * ((d - 1) * -1 % 2), (distance - 128) * ((d - 2) * -1 % 2)};
        }

        public static Asset DrawTrack(Track track) {
            return Asset.GetAsset(
                $"track:{track.Name}", () => {
                    Dictionary<Position, Section> grid = new Dictionary<Position, Section>();
                    int x = 0, y = 0, d = 0;

                    foreach (Section section in track.Sections) {
                        grid[new Position(x, y)] = section;
                        section.Dir = d;

                        if (section.Type == SectionType.LeftCorner) d--;
                        else if (section.Type == SectionType.RightCorner) d++;

                        //Non negative modulo (a % b only works for b >= 0)
                        d = (d %= 4) < 0 ? d + 4 : d;
                        x += (d - 1) * -1 % 2;
                        y += (d - 2) * -1 % 2;
                    }

                    Position min = grid.Keys.Aggregate(
                        (p1, p2) => new Position(Math.Min(p1.X, p2.X), Math.Min(p1.Y, p2.Y))
                    );
                    Position max = grid.Keys.Aggregate(
                        (p1, p2) => new Position(Math.Max(p1.X, p2.X), Math.Max(p1.Y, p2.Y))
                    );

                    int w = max.X - min.X + 1;
                    int h = max.Y - min.Y + 1;

                    Bitmap canvas = Asset.CreateEmpty(w * TileSize, h * TileSize);
                    Graphics graphics = Graphics.FromImage(canvas);

                    TrackAssetPack assets = new TrackAssetPack(TrackAssetPack.Type.Asphalt);

                    int cx = 0, cy = 0;
                    for (y = min.Y; y <= max.Y; y++) {
                        for (x = min.X; x <= max.X; x++) {
                            Position pos = new Position(x, y);
                            Section section = grid.ContainsKey(pos) ? grid[pos] : null;
                            cx++;
                            graphics.DrawImage(
                                Asset.Random().Image,
                                new Point((cx - 1) * TileSize, cy * TileSize)
                            );
                            if (section == null) continue;

                            RotatedAsset piece;
                            switch (section.Type) {
                                case SectionType.Straight: {
                                    piece = assets.Straight;
                                    break;
                                }
                                case SectionType.LeftCorner: {
                                    piece = assets.LeftTurn;
                                    break;
                                }
                                case SectionType.RightCorner: {
                                    piece = assets.RightTurn;
                                    break;
                                }
                                case SectionType.StartGrid: {
                                    piece = assets.Start;
                                    break;
                                }
                                case SectionType.Finish: {
                                    piece = assets.Finish;
                                    break;
                                }
                                default: throw new ArgumentOutOfRangeException();
                            }

                            graphics.DrawImage(
                                piece.Directed(section.Dir).Image, new Point((cx - 1) * TileSize, cy * TileSize)
                            );
                            // graphics.DrawString(
                            //     section.Dir.ToString(), new Font("Tahoma", 20), Brushes.Black,
                            //     new RectangleF((cx - 1) * TileSize, cy * TileSize, TileSize, TileSize)
                            // );
                        }

                        cx = 0;
                        cy++;
                    }

                    return canvas;
                }
            );
        }
    }
}