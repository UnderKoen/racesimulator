﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Controller;
using Model;
using VisualRaceSimulator.Annotations;

namespace VisualRaceSimulator {
    public class Test : INotifyPropertyChanged {
        public string RaceName => Data.CurrentRace.Track.Name;

        public List<ParticipantData<int>> Points => Data.Competition.Points.data.ToList();
        public List<ParticipantData<TimeSpan>> Times => Data.Competition.TotalTime.data.ToList();
        
        public List<ParticipantData<int>> Distance => Data.CurrentRace.Distance.data.ToList();
        public List<ParticipantData<TimeSpan>> RoundTimes => Data.CurrentRace.RoundTime.data.ToList();
        public List<ParticipantData<int>> Rounds => Data.CurrentRace.Rounds.data.ToList();

        public void DriverChanged(object o, RaceEventArgs args) { 
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(""));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}