﻿using System.Windows;

namespace VisualRaceSimulator {
    public partial class RaceInfo : Window {
        public RaceInfo() {
            InitializeComponent(); 
            MainWindow.change += ((Test) DataContext).DriverChanged;
        }
    }
}