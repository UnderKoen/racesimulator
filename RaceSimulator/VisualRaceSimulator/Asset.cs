﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Model;
using PixelFormat = System.Drawing.Imaging.PixelFormat;

namespace VisualRaceSimulator {
    public delegate Bitmap ImageBuilder();

    public class Asset {
        private const string Root = "../../Assets/";
        private static Dictionary<string, Asset> cache = new Dictionary<string, Asset>();

        public static Asset GetAsset(string key, ImageBuilder builder) {
            if (!cache.ContainsKey(key)) {
                cache[key] = new Asset(key, builder());
            }

            return cache[key];
        }

        public static Bitmap CreateEmpty(int w, int h) {
            Bitmap image = new Bitmap(w, h);
            return image;
        }

        public static Asset FromFile(string file) {
            string name = $"file:{file}";
            return GetAsset(
                name, () => {
                    Bitmap image = new Bitmap(file);
                    image.SetResolution(120F, 120F);
                    return image;
                }
            );
        }

        public static Asset FromAsset(string file) { return FromFile($"{Root}{file}"); }

        public static void Clear() { cache.Clear(); }

        public static Asset Combine(Asset[,] matrix, int w, int h, string name) {
            return GetAsset(
                name, () => {
                    int mw = matrix.GetLength(1);
                    int mh = matrix.GetLength(0);

                    Bitmap image = CreateEmpty(mw * w, mh * h);
                    Graphics graphics = Graphics.FromImage(image);

                    for (int y = 0; y < mh; y++) {
                        for (int x = 0; x < mw; x++) {
                            graphics.DrawImage(matrix[y, x].Image, new Point(x * w, y * h));
                        }
                    }

                    return image;
                }
            );
        }

        public string Name { get; }
        public Bitmap Image { get; }

        public Asset(string name, Bitmap image) {
            Name = name;
            Image = image;
        }

        public static BitmapSource CreateBitmapSourceFromGdiBitmap(Bitmap image) {
            Rectangle rect = new Rectangle(0, 0, image.Width, image.Height);

            BitmapData bitmapData = image.LockBits(rect, ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);

            try {
                int size = rect.Width * rect.Height * 4;

                return BitmapSource.Create(
                    image.Width,
                    image.Height,
                    image.HorizontalResolution,
                    image.VerticalResolution,
                    PixelFormats.Bgra32,
                    null,
                    bitmapData.Scan0,
                    size,
                    bitmapData.Stride
                );
            } finally {
                image.UnlockBits(bitmapData);
            }
        }

        public BitmapSource CreateBitmapSourceFromGdiBitmap() { return CreateBitmapSourceFromGdiBitmap(Image); }

        public Asset Rotate(int rotation) {
            int dir = rotation / 90;
            return GetAsset(
                $"{Name}_r{rotation}", () => {
                    int h = Image.Height;
                    int w = Image.Width;

                    Point[] matrix;
                    switch (dir) {
                        default: return Image;
                        case 1: {
                            matrix = new[] {new Point(h, 0), new Point(h, w), new Point(0, 0)};
                            w = Image.Height;
                            h = Image.Width;
                            break;
                        }
                        case 2:
                            matrix = new[] {new Point(w, h), new Point(0, h), new Point(w, 0)};
                            break;
                        case 3:
                            matrix = new[] {new Point(0, w), new Point(0, 0), new Point(h, w)};
                            w = Image.Height;
                            h = Image.Width;
                            break;
                    }

                    Bitmap rotatedImage = new Bitmap(w, h);
                    rotatedImage.SetResolution(Image.HorizontalResolution, Image.VerticalResolution);

                    using (Graphics g = Graphics.FromImage(rotatedImage)) {
                        g.DrawImage(Image, matrix);
                    }

                    return rotatedImage;
                }
            );
        }

        public static Asset Grass = FromAsset("Tiles/Grass/land_grass11.png");
        public static Asset GrassAlt = FromAsset("Tiles/Grass/land_grass04.png");

        public static Random _random = new Random();

        public static Asset Random() {
            if (_random.Next(10) == 0) {
                return Combine(
                    new[,] {
                        {FromAsset("Tiles/Grass/land_grass01.png"), FromAsset("Tiles/Grass/land_grass02.png")},
                        {FromAsset("Tiles/Grass/land_grass06.png"), FromAsset("Tiles/Grass/land_grass07.png")}
                    },
                    128,
                    128,
                    $"Grass_Pit"
                );
            }
            
            return Combine(
                new[,] {
                    {_random.Next(2) == 0 ? Grass : GrassAlt, _random.Next(2) == 0 ? Grass : GrassAlt},
                    {_random.Next(2) == 0 ? Grass : GrassAlt, _random.Next(2) == 0 ? Grass : GrassAlt}
                },
                128,
                128,
                $"{_random.Next(100)}_Random"
            );
        }

        public static Dictionary<TeamColor, RotatedAsset> Cars = new Dictionary<TeamColor, RotatedAsset> {
            {TeamColor.Blue, new RotatedAsset(FromAsset("Cars/car_blue_small_1.png"))},
            {TeamColor.Green, new RotatedAsset(FromAsset("Cars/car_green_small_1.png"))},
            {TeamColor.Grey, new RotatedAsset(FromAsset("Cars/car_black_small_1.png"))},
            {TeamColor.Red, new RotatedAsset(FromAsset("Cars/car_red_small_1.png"))},
            {TeamColor.Yellow, new RotatedAsset(FromAsset("Cars/car_yellow_small_1.png"))},
        };
        
        public static Asset Oil = FromAsset("Objects/oil.png");
    }

    public class RotatedAsset {
        private readonly Asset _asset;
        private readonly int _rotation;

        public RotatedAsset(Asset asset, int rotation = 0) {
            _asset = asset;
            _rotation = rotation;
        }

        public Asset Directed(int dir) {
            switch (dir) {
                case 0: return _asset.Rotate(_rotation + 90);
                case 1: return _asset.Rotate(_rotation + 180);
                case 2: return _asset.Rotate(_rotation + 270);
                case 3: return _asset;
                default: throw new ArgumentOutOfRangeException(nameof(dir), dir, null);
            }
        }
    }

    public class TrackAssetPack {
        public enum Type {
            Asphalt, Dirt, Sand
        }

        private readonly Type _type;

        public TrackAssetPack(Type type) { _type = type; }

        private static string _rootFolder(Type type) {
            switch (type) {
                case Type.Asphalt: return "Tiles/Asphalt road/";
                case Type.Dirt: return "Tiles/Dirt road/";
                case Type.Sand: return "Tiles/Sand road/";
                default: throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        private static string _fileName(Type type, string code) {
            switch (type) {
                case Type.Asphalt: return $"road_asphalt{code}.png";
                case Type.Dirt: return $"road_dirt{code}.png";
                case Type.Sand: return $"road_sand{code}.png";
                default: throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        private static Asset _file(Type type, string code) {
            return Asset.FromAsset($"{_rootFolder(type)}{_fileName(type, code)}");
        }

        private Asset _file(string code) => _file(_type, code);

        public RotatedAsset Straight =>
            new RotatedAsset(
                Asset.Combine(
                    new[,] {
                        {_file("21"), _file("23")},
                        {_file("21"), _file("23")}
                    },
                    128,
                    128,
                    $"{_type.ToString()}_Straight"
                )
            );

        public RotatedAsset Finish =>
            new RotatedAsset(
                Asset.Combine(
                    new[,] {
                        {_file("69"), _file("71")},
                        {_file("21"), _file("23")}
                    },
                    128,
                    128,
                    $"{_type.ToString()}_Finish"
                )
            );

        public RotatedAsset RightTurn =>
            new RotatedAsset(
                Asset.Combine(
                    new[,] {
                        {_file("06"), _file("04")},
                        {_file("21"), _file("27")}
                    },
                    128,
                    128,
                    $"{_type.ToString()}_RightTurn"
                )
            );

        public RotatedAsset LeftTurn =>
            new RotatedAsset(
                Asset.Combine(
                    new[,] {
                        {_file("04"), _file("07")},
                        {_file("28"), _file("23")}
                    },
                    128,
                    128,
                    $"{_type.ToString()}_LeftTurn"
                )
            );

        public RotatedAsset Start =>
            new RotatedAsset(
                Asset.Combine(
                    new[,] {
                        {_file("21"), _file("23")},
                        {_file("91"), _file("95")}
                    },
                    128,
                    128,
                    $"{_type.ToString()}_Start"
                )
            );
    }
}