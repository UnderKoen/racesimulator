﻿using System;
using System.Threading;
using Controller;

namespace RaceSimulator {
    internal static class Program {
        public static void Main(string[] args) {
            Data.Init();
            NextRace();
            while (true) {
            }
        }

        public static void StartRace(Race race) {
            DrawCurrent();
            race.DriverChanged += (o, eventArgs) => DrawCurrent();
            race.RaceEnded += (o, eventArgs) => {
                race.Stop();
                NextRace();
            };
            race.Start();
        }

        public static void NextRace() {
            Console.Clear();
            if (Data.HasNextRace()) {
                Data.NextRace();
                StartRace(Data.CurrentRace);
            } else {
                DrawCurrent();
            }
        }

        public static void DrawCurrent() {
            Visualization.DrawRace(
                Data.CurrentRace,
                Data.Competition.GetBestPoints(),
                Data.CurrentRace.GetFarthest(),
                Data.CurrentRace.GetBestRound(),
                Data.Competition.GetTotalTime()
            );
        }
    }
}