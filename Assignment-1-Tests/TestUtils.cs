﻿using System.Collections.Generic;
using System.Linq;

namespace Assignment_1_Tests {
    public class TestUtils {
        // ------------------------------------------------------------
        // Maakt een lijst van een string. Getallen zijn ints,
        // gescheiden door een spatie
        // ------------------------------------------------------------
        public static List<int> ListFromString(string str) {
            List<int> list = new List<int>();

            if (str.Length > 0) list = str.Split(' ').Select(int.Parse).ToList();
            return list;
        }
    }
}